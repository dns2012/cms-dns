<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $css        = asset('/public/assets/css').'/';
        $js         = asset('/public/assets/js').'/';
        $font       = asset('/public/assets/font').'/';
        $tim        = url('/timthumb.php?src=');
        $imageAdmin = 'http://localhost/resources/admin/';
        $imagePosts = 'http://localhost/resources/posts/';
        view::share('css', $css);
        view::share('js', $js);
        view::share('font', $font);
        view::share('tim', $tim);
        view::share('imageAdmin', $imageAdmin);
        view::share('$imagePosts', $imagePosts);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
