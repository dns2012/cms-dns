<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    protected $table = 'administrator';
    public $timestamps = false;


    function getAllAdministrator() {
      $data = Administrator::get();
      return $data;
    }

    function addAdministrator($data) {
      Administrator::insert($data);
    }

    function getAdministrator($id) {
      $data = Administrator::where('id', $id)
              ->first();
      return $data;
    }

    function editAdministrator($id, $data) {
      Administrator::where('id', $id)
      ->update($data);
    }

    function deleteAdministrator($id) {
      Administrator::where('id', $id)
      ->delete();
    }
}
