<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Administrator;


class AdministratorController extends Controller
{

    function index() {
      $adminModel = new Administrator;
      $administrator = $adminModel->getAllAdministrator();
      return view('administrator.index', [
        'page'          =>  'Administrator',
        'administrator' =>  $administrator
      ]);
    }

    function addAdministrator() {
      $adminModel = new Administrator;
      return view('administrator.add', [
        'page'          =>  'Administrator'
      ]);
    }

    function addActionAdministrator(Request $request) {
      $adminModel = new Administrator;
      if(isset($_FILES['photo'])){
          $file_name = date('YmdHis').$_FILES['photo']['name'];
          $file_size =$_FILES['photo']['size'];
          $file_tmp =$_FILES['photo']['tmp_name'];
          $file_type=$_FILES['photo']['type'];
          move_uploaded_file($file_tmp,"/Applications/XAMPP/htdocs/resources/admin/".$file_name);
       }

       $data = [
         'name'       =>  $request->input('name'),
         'bio'        =>  $request->input('bio'),
         'photo'      =>  $file_name,
         'email'      =>  $request->input('email'),
         'password'   =>  md5($request->input('password')),
         'active'     =>  $request->input('active'),
         'level'      =>  $request->input('level'),
         'registered' =>  date('Y-m-d H:i:s')
       ];

       $adminModel->addAdministrator($data);

       return redirect()->action('AdministratorController@index');
    }

    function editAdministrator($id) {
      $adminModel = new Administrator;
      $administrator = $adminModel->getAdministrator($id);
      return view('administrator.edit', [
        'page'          =>  'Administrator',
        'administrator' =>  $administrator
      ]);
    }

    function editActionAdministrator(Request $request, $id) {
      $adminModel = new Administrator;
      if(!empty($_FILES['photo']['name'])){
          $file_name = date('YmdHis').$_FILES['photo']['name'];
          $file_size =$_FILES['photo']['size'];
          $file_tmp =$_FILES['photo']['tmp_name'];
          $file_type=$_FILES['photo']['type'];
          move_uploaded_file($file_tmp,"/Applications/XAMPP/htdocs/resources/admin/".$file_name);
       } else {
         $file_name = $request->input('oldphoto');
       }

       if(!empty($request->input('password'))) {
         $password = md5($request->input('password'));
       } else {
         $password = $request->input('oldpassword');
       }

       $data = [
         'name'       =>  $request->input('name'),
         'bio'        =>  $request->input('bio'),
         'photo'      =>  $file_name,
         'email'      =>  $request->input('email'),
         'password'   =>  $password,
         'active'     =>  $request->input('active'),
         'level'      =>  $request->input('level'),
         'registered' =>  date('Y-m-d H:i:s')
       ];

       $adminModel->editAdministrator($id, $data);

       return redirect()->action('AdministratorController@index');
    }

    function deleteAdministrator($id) {
      $adminModel = new Administrator;

      $adminModel->deleteAdministrator($id);

      return redirect()->action('AdministratorController@index');
    }
}
