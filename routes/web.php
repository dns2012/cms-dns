<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// DASHBOARD
Route::get('/dashboard', 'DashboardController@index');

// POST
Route::get('/posts', 'PostsController@index');
Route::get('/posts/add', 'PostsController@addPost');
Route::post('/posts/addaction', 'PostsController@addActionPost');
Route::get('/posts/edit/{id}', 'PostsController@editPost');
Route::post('/posts/editaction/{id}', 'PostsController@editActionPost');
Route::get('/posts/delete/{id}', 'PostsController@deletePost');

// CATEGORY
Route::get('/category', 'CategoryController@index');
Route::get('/category/add', 'CategoryController@addCategory');
Route::post('/category/addaction', 'CategoryController@addActionCategory');
Route::get('/category/edit/{id}', 'CategoryController@editCategory');
Route::post('/category/editaction/{id}', 'CategoryController@editActionCategory');
Route::get('/category/delete/{id}', 'CategoryController@deleteCategory');

// Administrator
Route::get('/administrator', 'AdministratorController@index');
Route::get('/administrator/add', 'AdministratorController@addAdministrator');
Route::post('/administrator/addaction', 'AdministratorController@addActionAdministrator');
Route::get('/administrator/edit/{id}', 'AdministratorController@editAdministrator');
Route::post('/administrator/editaction/{id}', 'AdministratorController@editActionAdministrator');
Route::get('/administrator/delete/{id}', 'AdministratorController@deleteAdministrator');
