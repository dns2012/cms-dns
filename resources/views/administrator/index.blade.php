@include('layouts.header')

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{url('/dashboard')}}">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Administrator</li>
			</ol>
		</div><!--/.row-->
		<br>


		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="{{url('/administrator/add')}}"><button class="btn btn-primary" title="Add Administrator"><span class="fa fa-plus"></span></button></a>
					</div>
					<div class="panel-body">
            <div class="table-responsive">
						  <table class="table">
						    <thead>
						      <tr>
						        <th>#</th>
						        <th>Photo</th>
						        <th>Name</th>
						        <th>Active</th>
						        <th>Level</th>
						        <th>Option</th>
						      </tr>
						    </thead>
						    <tbody>
									@php $i = 1; @endphp
									@foreach($administrator as $administrator)
							      <tr>
							        <td>{{$i}}</td>
							        <td><img src="{{$tim.$imageAdmin.$administrator->photo}}&w=100&h=100" class="img-rounded" alt="Cinque Terre"></td>
							        <td>{{$administrator->name}}</td>
							        <td>@if($administrator->active == 'Y') <div class="btn btn-success">YES</div> @else <div class="btn btn-danger">NO</div> @endif</td>
							        <td>{{strtoupper($administrator->level)}}</td>
							        <td>
												<a href="{{url('/administrator/edit/'.$administrator->id)}}"><button class="btn btn-info" title="Edit Administrator"><span class="fa fa-edit"></span></button></a>
												<button class="btn btn-danger" title="Delete Administrator" data-toggle="modal" data-target="#myModal{{$i}}"><span class="fa fa-trash"></span></button>
											</td>
							      </tr>

										<div id="myModal{{$i}}" class="modal fade" role="dialog">
										  <div class="modal-dialog">

										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Delete Confirmation</h4>
										      </div>
										      <div class="modal-body">
										        <p>Are you sure delete this item?</p>
										      </div>
										      <div class="modal-footer">
														<a href="{{url('/administrator/delete/'.$administrator->id)}}"><button type="button" class="btn btn-danger">Yes</button></a>
										        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										      </div>
										    </div>

										  </div>
										</div>

									@php $i++; @endphp
									@endforeach
						    </tbody>
						  </table>
  					</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

@include('layouts.footer')
