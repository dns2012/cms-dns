@include('layouts.header')

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{url('/dashboard')}}">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Administrator</li>
        <li class="active">Add</li>
			</ol>
		</div><!--/.row-->
    <br>

		<div class="row">
			<div class="col-xs-12 col-md-12 col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
            <form action="{{url('/administrator/addaction')}}" method="POST" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="form-group">
                <label>Name : *</label>
                <input type="text" class="form-control" placeholder="Name" name="name" required>
              </div>
              <div class="form-group">
                <label>Biography : *</label>
                <textarea class="form-control" placeholder="About You" name="bio"style="height:200px" required></textarea>
              </div>
              <div class="form-group">
                <label>Email : *</label>
                <input type="email" class="form-control" placeholder="Email" name="email" required>
              </div>
              <div class="form-group">
                <label>Password : *</label>
                <input type="password" class="form-control" placeholder="Password" name="password" required>
              </div>
              <div class="form-group">
                <label>Level : *</label>
                <select class="form-control" name="level" required>
                  <option value="">--</option>
                  <option value="administrator">Administrator</option>
                  <option value="editor">Editor</option>
                  <option value="author">Author</option>
                </select>
              </div>
              <div class="form-group">
                <label>Active : *</label><br>
                <label class="radio-inline">
                  <input type="radio" name="active" value="Y" required>YES
                </label>
                <label class="radio-inline">
                  <input type="radio" name="active" value="N" required>NO
                </label>
              </div>
              <div class="form-group">
                <label>Photo : *</label>
                <input type="file" class="form-control" name="photo" required>
              </div>
              <div class="form-group">
              <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

@include('layouts.footer')
