@include('layouts.header')

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{url('/dashboard')}}">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Administrator</li>
        <li class="active">Edit</li>
			</ol>
		</div><!--/.row-->
    <br>

		<div class="row">
			<div class="col-xs-12 col-md-12 col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
            <form action="{{url('/administrator/editaction/'.$administrator->id)}}" method="POST" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="form-group">
                <label>Name : *</label>
                <input type="text" class="form-control" placeholder="Name" name="name" value="{{$administrator->name}}" required>
              </div>
              <div class="form-group">
                <label>Biography : *</label>
                <textarea class="form-control" placeholder="About You" name="bio"style="height:200px" required>{{$administrator->bio}}</textarea>
              </div>
              <div class="form-group">
                <label>Email : *</label>
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{$administrator->email}}" required>
              </div>
              <div class="form-group">
                <label>Password : *</label>
                <input type="password" class="form-control" placeholder="Password" name="password">
              </div>
              <div class="form-group">
                <label>Level : *</label>
                <select class="form-control" name="level" required>
                  <option value="">--</option>
                  <option @if($administrator->level == 'administrator') selected @endif value="administrator">Administrator</option>
                  <option @if($administrator->level == 'editor') selected @endif value="editor">Editor</option>
                  <option @if($administrator->level == 'author') selected @endif value="author">Author</option>
                </select>
              </div>
              <div class="form-group">
                <label>Active : *</label><br>
                <label class="radio-inline">
                  <input type="radio" name="active" value="Y" @if($administrator->active == 'Y') checked @endif required>YES
                </label>
                <label class="radio-inline">
                  <input type="radio" name="active" value="N" @if($administrator->active == 'N') checked @endif required required>NO
                </label>
              </div>
              <div class="form-group">
                <label>Photo : *</label><br>
                <img src="{{$tim.$imageAdmin.$administrator->photo}}&w=100&h=100" class="img-rounded" alt="Cinque Terre" style="margin-bottom:5px">
                <input type="file" class="form-control" name="photo">
              </div>
              <div class="form-group">
                <input type="hidden" name="oldpassword" value="{{$administrator->password}}">
                <input type="hidden" name="oldphoto" value="{{$administrator->photo}}">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

@include('layouts.footer')
